﻿namespace SGMMR.Models
{
    public class TipoMoneda
    {
        public int ID { get; set; }
        public string Descripcion { get; set; }
        public decimal ValorRd { get; set; }
        public string UserId { get; set; }
    }
}