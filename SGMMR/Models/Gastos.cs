﻿using System;

namespace SGMMR.Models
{
    public class Gastos
    {
        public int ID { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
        public DateTime Fecha { get; set; }
        public int CategoriaGastosId { get; set; }
        public int TipoMonedaId { get; set; }
        public int CuentaId { get; set; }
        public decimal Valortotal { get; set; }
        public CategoriaGastos categoriaGastos { get; set; }
        public Cuentas Cuenta { get; set; }
        public TipoMoneda TipoMoneda { get; set; }
        public string UserId { get; set; }
    }
}