﻿namespace SGMMR.Models
{
    public class CategoriaGastos
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string UserId { get; set; }
    }
}