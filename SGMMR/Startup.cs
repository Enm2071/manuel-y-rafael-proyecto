﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SGMMR.Startup))]
namespace SGMMR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
