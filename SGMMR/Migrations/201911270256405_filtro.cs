namespace SGMMR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class filtro : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoriaGastos", "UserId", c => c.String());
            AddColumn("dbo.CategoriaIngresos", "UserId", c => c.String());
            AddColumn("dbo.Cuentas", "UserId", c => c.String());
            AddColumn("dbo.Gastos", "UserId", c => c.String());
            AddColumn("dbo.TipoMonedas", "UserId", c => c.String());
            AddColumn("dbo.Ingresos", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ingresos", "UserId");
            DropColumn("dbo.TipoMonedas", "UserId");
            DropColumn("dbo.Gastos", "UserId");
            DropColumn("dbo.Cuentas", "UserId");
            DropColumn("dbo.CategoriaIngresos", "UserId");
            DropColumn("dbo.CategoriaGastos", "UserId");
        }
    }
}
