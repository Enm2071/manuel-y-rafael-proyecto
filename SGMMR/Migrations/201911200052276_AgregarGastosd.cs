namespace SGMMR.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AgregarGastosd : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            CreateTable(
                "dbo.CategoriaGastos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cuentas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                        EntidadBancaria = c.String(),
                        TipoCuenta = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Gastos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                        Monto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Fecha = c.DateTime(nullable: false),
                        CategoriaGastosId = c.Int(nullable: false),
                        TipoMonedaId = c.Int(nullable: false),
                        CuentaId = c.Int(nullable: false),
                        Valortotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CategoriaGastos", t => t.CategoriaGastosId, cascadeDelete: true)
                .ForeignKey("dbo.Cuentas", t => t.CuentaId, cascadeDelete: true)
                .ForeignKey("dbo.TipoMonedas", t => t.TipoMonedaId, cascadeDelete: true)
                .Index(t => t.CategoriaGastosId)
                .Index(t => t.TipoMonedaId)
                .Index(t => t.CuentaId);
            
            AddColumn("dbo.Ingresos", "CuentaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Ingresos", "CuentaId");
            CreateIndex("dbo.AspNetUsers", "UserName", unique: true, name: "UserNameIndex");
            AddForeignKey("dbo.Ingresos", "CuentaId", "dbo.Cuentas", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ingresos", "CuentaId", "dbo.Cuentas");
            DropForeignKey("dbo.Gastos", "TipoMonedaId", "dbo.TipoMonedas");
            DropForeignKey("dbo.Gastos", "CuentaId", "dbo.Cuentas");
            DropForeignKey("dbo.Gastos", "CategoriaGastosId", "dbo.CategoriaGastos");
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Ingresos", new[] { "CuentaId" });
            DropIndex("dbo.Gastos", new[] { "CuentaId" });
            DropIndex("dbo.Gastos", new[] { "TipoMonedaId" });
            DropIndex("dbo.Gastos", new[] { "CategoriaGastosId" });
            DropColumn("dbo.Ingresos", "CuentaId");
            DropTable("dbo.Gastos");
            DropTable("dbo.Cuentas");
            DropTable("dbo.CategoriaGastos");
       
        }
    }
}
