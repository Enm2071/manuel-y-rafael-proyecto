﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SGMMR.Models;

namespace SGMMR.Controllers
{
    [Authorize]
    public class TipoMonedasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly UserManager<ApplicationUser> userManager;
        public TipoMonedasController()
        {
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

        }
        // GET: TipoMonedas
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var user = userManager.FindByName(userName);
            return View(db.TipoMoneda.Where(c=>c.UserId == user.Id).ToList());
        }

        // GET: TipoMonedas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMoneda.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoMonedas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,ValorRd")] TipoMoneda tipoMoneda)
        {
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = userManager.FindByName(userName);
                tipoMoneda.UserId = user.Id;
                db.TipoMoneda.Add(tipoMoneda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMoneda.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            return View(tipoMoneda);
        }

        // POST: TipoMonedas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,ValorRd")] TipoMoneda tipoMoneda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoMoneda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoMoneda);
        }

        // GET: TipoMonedas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMoneda tipoMoneda = db.TipoMoneda.Find(id);
            if (tipoMoneda == null)
            {
                return HttpNotFound();
            }
            return View(tipoMoneda);
        }

        // POST: TipoMonedas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoMoneda tipoMoneda = db.TipoMoneda.Find(id);
            db.TipoMoneda.Remove(tipoMoneda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
