﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SGMMR.Models;

namespace SGMMR.Controllers
{
    [Authorize]
    public class GastosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly UserManager<ApplicationUser> userManager;
        public GastosController()
        {
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

        }

        // GET: Gastos
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var user = userManager.FindByName(userName);
            var gastos = db.Gastos.Include(g => g.categoriaGastos).Include(g => g.Cuenta).Include(g => g.TipoMoneda);
            return View(gastos.ToList());
        }

        // GET: Gastos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gastos gastos = db.Gastos.Find(id);
            if (gastos == null)
            {
                return HttpNotFound();
            }
            return View(gastos);
        }

        // GET: Gastos/Create
        public ActionResult Create()
        {
            var validacionExistenciaDeCuentas = db.Cuentas.Any();
            if (!validacionExistenciaDeCuentas) return View("ErrorExistenciaCuenta");
            ViewBag.CategoriaGastosId = new SelectList(db.CategoriaGastos, "Id", "Descripcion");
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion");
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion");
            return View();
        }

        // POST: Gastos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Monto,Fecha,CategoriaGastosId,TipoMonedaId,CuentaId,Valortotal")] Gastos gastos)
        {
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = userManager.FindByName(userName);
                gastos.UserId = user.Id;
                db.Gastos.Add(gastos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaGastosId = new SelectList(db.CategoriaGastos, "Id", "Descripcion", gastos.CategoriaGastosId);
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", gastos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", gastos.TipoMonedaId);
            return View(gastos);
        }

        // GET: Gastos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gastos gastos = db.Gastos.Find(id);
            if (gastos == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaGastosId = new SelectList(db.CategoriaGastos, "Id", "Descripcion", gastos.CategoriaGastosId);
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", gastos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", gastos.TipoMonedaId);
            return View(gastos);
        }

        // POST: Gastos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Monto,Fecha,CategoriaGastosId,TipoMonedaId,CuentaId,Valortotal")] Gastos gastos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gastos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaGastosId = new SelectList(db.CategoriaGastos, "Id", "Descripcion", gastos.CategoriaGastosId);
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", gastos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", gastos.TipoMonedaId);
            return View(gastos);
        }

        // GET: Gastos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gastos gastos = db.Gastos.Find(id);
            if (gastos == null)
            {
                return HttpNotFound();
            }
            return View(gastos);
        }

        // POST: Gastos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gastos gastos = db.Gastos.Find(id);
            db.Gastos.Remove(gastos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
