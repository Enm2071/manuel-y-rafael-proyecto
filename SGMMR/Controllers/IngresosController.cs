﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SGMMR.Models;

namespace SGMMR.Controllers
{
    [Authorize]
    public class IngresosController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        private readonly UserManager<ApplicationUser> userManager;
        public IngresosController()
        {
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

        }
        // GET: Ingresos
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var user = userManager.FindByName(userName);
            var ingresos = db.Ingresos.Include(i => i.Cuenta).Include(i => i.TipoMoneda);
            return View(ingresos.Where(c=>c.UserId == user.Id).ToList());
        }

        // GET: Ingresos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingresos ingresos = db.Ingresos.Find(id);
            if (ingresos == null)
            {
                return HttpNotFound();
            }
            return View(ingresos);
        }

        // GET: Ingresos/Create
        public ActionResult Create()
        {
            var validacionExistenciaDeCuentas = db.Cuentas.Any();
            if (!validacionExistenciaDeCuentas) return View("ErrorCuenta");
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion");
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion");
            return View();
        }

        // POST: Ingresos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Monto,Fecha,CategoriaIngresoId,TipoMonedaId,CuentaId,Valortotal")] Ingresos ingresos)
        {
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = userManager.FindByName(userName);
                ingresos.UserId = user.Id;
                db.Ingresos.Add(ingresos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", ingresos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", ingresos.TipoMonedaId);
            return View(ingresos);
        }

        // GET: Ingresos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingresos ingresos = db.Ingresos.Find(id);
            if (ingresos == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", ingresos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", ingresos.TipoMonedaId);
            return View(ingresos);
        }

        // POST: Ingresos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Monto,Fecha,CategoriaIngresoId,TipoMonedaId,CuentaId,Valortotal")] Ingresos ingresos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ingresos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "Descripcion", ingresos.CuentaId);
            ViewBag.TipoMonedaId = new SelectList(db.TipoMoneda, "ID", "Descripcion", ingresos.TipoMonedaId);
            return View(ingresos);
        }

        // GET: Ingresos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ingresos ingresos = db.Ingresos.Find(id);
            if (ingresos == null)
            {
                return HttpNotFound();
            }
            return View(ingresos);
        }

        // POST: Ingresos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ingresos ingresos = db.Ingresos.Find(id);
            db.Ingresos.Remove(ingresos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
