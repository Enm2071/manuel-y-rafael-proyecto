﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SGMMR.Models;

namespace SGMMR.Controllers
{
    [Authorize]
    public class CategoriaIngresosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly UserManager<ApplicationUser> userManager;
        public CategoriaIngresosController()
        {
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

        }

        // GET: CategoriaIngresos
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var user = userManager.FindByName(userName);
            return View(db.CategoriaIngresos.Where(c=>c.UserId == user.Id).ToList());
        }

        // GET: CategoriaIngresos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaIngresos categoriaIngresos = db.CategoriaIngresos.Find(id);
            if (categoriaIngresos == null)
            {
                return HttpNotFound();
            }
            return View(categoriaIngresos);
        }

        // GET: CategoriaIngresos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoriaIngresos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Descripcion")] CategoriaIngresos categoriaIngresos)
        {
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = userManager.FindByName(userName);
                categoriaIngresos.UserId = user.Id;
                db.CategoriaIngresos.Add(categoriaIngresos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoriaIngresos);
        }

        // GET: CategoriaIngresos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaIngresos categoriaIngresos = db.CategoriaIngresos.Find(id);
            if (categoriaIngresos == null)
            {
                return HttpNotFound();
            }
            return View(categoriaIngresos);
        }

        // POST: CategoriaIngresos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Descripcion")] CategoriaIngresos categoriaIngresos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoriaIngresos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoriaIngresos);
        }

        // GET: CategoriaIngresos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaIngresos categoriaIngresos = db.CategoriaIngresos.Find(id);
            if (categoriaIngresos == null)
            {
                return HttpNotFound();
            }
            return View(categoriaIngresos);
        }

        // POST: CategoriaIngresos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoriaIngresos categoriaIngresos = db.CategoriaIngresos.Find(id);
            db.CategoriaIngresos.Remove(categoriaIngresos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
